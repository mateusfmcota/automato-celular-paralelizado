# Automato Celular Paralelizado

Algoritmo do trabalho final de Programação Concorrente

## Instruções de compilação

Sera necessario as seguites bibliotecas:
 * pthreads
 * SDL2

 Compilar com o seguinte comando:

 > g++ -out GameOfLife.cpp -lSDL2 -lpthreads


## informações

A função automata(int x, int y); é a função que faz o automato em cada celula

A função lambda set(int x, int y, wstring s); em main() é uma função para ajudar a criar o mapa do automato


