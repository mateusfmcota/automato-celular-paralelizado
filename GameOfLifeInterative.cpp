#include <SDL2/SDL.h>
#include <thread>
#include <chrono>
#include <string>

#define CELL_SIZE 4
#define WINDOW_WIDTH 240
#define WINDOW_HEIGHT 160
#define DELAY 20

#define NTHREADS 8

int x, y;
void drawRoutine();

const int sz = (WINDOW_WIDTH * WINDOW_HEIGHT);
//Array
int m_output[WINDOW_WIDTH][WINDOW_HEIGHT] = {0};
int m_state[WINDOW_WIDTH][WINDOW_HEIGHT] = {0};

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Color grid_background = {22, 22, 22, 255};      // Barely Black
SDL_Color grid_cursor_color = {255, 255, 255, 255}; // White

void automata(int x, int y)
{
    int nVizinhos = m_output[(x - 1)][(y - 1)] + m_output[(x - 0)][(y - 1)] + m_output[(x + 1)][(y - 1)] +
                    m_output[(x - 1)][(y - 0)] + 0 + m_output[(x + 1)][(y - 0)] +
                    m_output[(x - 1)][(y + 1)] + m_output[(x - 0)][(y + 1)] + m_output[(x + 1)][(y + 1)];

    if (m_output[x][y] == 1)
        m_state[x][y] = (nVizinhos == 2 || nVizinhos == 3) ? 1 : 0;
    else
        m_state[x][y] = (nVizinhos == 3) ? 1 : 0;
}

void exec()
{
    int counter;
    if (x == 1 && y == 1)
    {
        for (int i = 0; i < WINDOW_WIDTH; i++)
        {
            for (int j = 0; j < WINDOW_HEIGHT; j++)
            {
                m_output[i][j] = m_state[i][j];
            }
        }
    }
    if (x == WINDOW_WIDTH - 1)
    {
        y++;
        x = 0;
    }
    if (y == WINDOW_HEIGHT)
    {
        // printf("WH: %d,%d\n", x, y);
        x = 1;
        y = 1;
        drawRoutine();
        counter++;
    }
    x++;



    automata(x, y);
    
}

void gameOfLife()
{

    for (int i = 1; i < WINDOW_WIDTH; i++)
    {
        for (int j = 1; j < WINDOW_HEIGHT; j++)
        {
            automata(i, j);
            //     //Calcular o numero de vizinhos
            //     int nVizinhos = m_output[(i - 1)][(j - 1)] + m_output[(i - 0)][(j - 1)] + m_output[(i + 1)][(j - 1)] +
            //                     m_output[(i - 1)][(j - 0)] + 0 + m_output[(i + 1)][(j - 0)] +
            //                     m_output[(i - 1)][(j + 1)] + m_output[(i - 0)][(j + 1)] + m_output[(i + 1)][(j + 1)];

            //     if (m_output[i][j] == 1)
            //         m_state[i][j] = (nVizinhos == 2 || nVizinhos == 3) ? 1 : 0;
            //     else
            //         m_state[i][j] = (nVizinhos == 3) ? 1 : 0;
        }
    }
}

void drawRoutine()
{
    for (int i = 0; i < WINDOW_WIDTH; i++)
    {
        for (int j = 0; j < WINDOW_HEIGHT; j++)
        {
            m_output[i][j] = m_state[i][j];
        }
    }

    // Desenha o fundo
    SDL_SetRenderDrawColor(renderer, grid_background.r, grid_background.g,
                           grid_background.b, grid_background.a);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, grid_cursor_color.r,
                           grid_cursor_color.g, grid_cursor_color.b,
                           grid_cursor_color.a);
    //Desenha os quadrados
    for (int i = 1; i < WINDOW_WIDTH; i++)
    {
        for (int j = 1; j < WINDOW_HEIGHT; j++)
        {
            // printf("%d {%d,%d,%d} \n",i,((i % WINDOW_WIDTH)),((i / WINDOW_HEIGHT)),CELL_SIZE);
            // printf("Entrou \n");
            //Desenha apenas se M == 1

            if (m_state[i][j] == 1)
            {
                SDL_Rect gd;
                gd.x = i * CELL_SIZE + 1;
                gd.y = j * CELL_SIZE + 1;
                gd.w = CELL_SIZE;
                gd.h = CELL_SIZE;
                SDL_RenderFillRect(renderer, &gd);
            }
        }
    }

    SDL_RenderPresent(renderer);
    std::this_thread::sleep_for(std::chrono::milliseconds(DELAY));
}

void initVideo()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Initialize SDL: %s",
                     SDL_GetError());
    }
}

void createRenderer(int window_width, int window_height)
{
    if (SDL_CreateWindowAndRenderer(window_width, window_height, 0, &window,
                                    &renderer) < 0)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "Create window and renderer: %s", SDL_GetError());
    }

    SDL_SetWindowTitle(window, "Game of Life");
}

int main()
{
    int window_width = (WINDOW_WIDTH * CELL_SIZE);
    int window_height = (WINDOW_HEIGHT * CELL_SIZE);

    initVideo();
    createRenderer(window_width, window_height);

    auto set = [&](int x, int y, std::wstring s) {
        int p = 0;
        for (auto c : s)
        {
            m_state[x + p][y] = c == L'#' ? 1 : 0;
            p++;
        }
    };

    set(60,100, L"..##.");
    set(60,101, L".##..");
    set(60,102, L"..#..");

    // set(130,120, L"..##.");
    // set(130,121, L".##..");
    // set(130,122, L"..#..");

    // set(20,20, L".......#");
    // set(20,21, L".##");
    // set(20,22, L"..#...###");

    // set(70,60, L"..#");
    // set(70,61, L"....#");
    // set(70,62, L".##..###");

    // set(190,60, L"#####");
    // set(190,61, L"#....#.......##");
    // set(190,62, L"#...........##.###");
    // set(190,63, L".#.........##.####");
    // set(190,64, L"...##...##.##..##");
    // set(190,65, L".....#....#..#");
    // set(190,66, L"......#.#.#.#");
    // set(190,67, L".......#");
    // set(190,68, L".......#");
    // set(190,69, L"......#.#.#.#");
    // set(190,70, L".....#....#..#");
    // set(190,71, L"...##...##.##..##");
    // set(190,72, L".#.........##.####");
    // set(190,73, L"#...........##.###");
    // set(190,74, L"#....#.......##");
    // set(190,75, L"#####");

    set(60, 45, L"........................#............");
    set(60, 46, L"......................#.#............");
    set(60, 47, L"............##......##............##.");
    set(60, 48, L"...........#...#....##............##.");
    set(60, 49, L"##........#.....#...##...............");
    set(60, 50, L"##........#...#.##....#.#............");
    set(60, 51, L"..........#.....#.......#............");
    set(60, 52, L"...........#...#.....................");
    set(60, 53, L"............##.......................");


    // drawRoutine();

    SDL_bool quit = SDL_FALSE;

    while (!quit)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit = SDL_TRUE;
                break;
            }
        }
		drawRoutine();
        gameOfLife();
        std::this_thread::sleep_for(std::chrono::milliseconds(DELAY));
    }

    exec();
    // getchar();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return EXIT_SUCCESS;
}
