#include <SDL2/SDL.h>
#include <thread>
#include <chrono>
#include <string>

#include "pthread.h"

typedef struct
{
    int start, end;
} t_Args;

void drawRoutine();
int itCounter;

//Threading
#define NTHREADS 8
#define THREADSTOTAL ((NTHREADS == 0) ? std::thread::hardware_concurrency() : NTHREADS)
#define ITER 10000

pthread_mutex_t mut, bar, cnt;
pthread_cond_t cond;

SDL_bool quit = SDL_FALSE;

int counter;
int x, y;

#define CELL_SIZE 4
#define WINDOW_WIDTH 470
#define WINDOW_HEIGHT 250
#define DELAY 5

const int sz = (WINDOW_WIDTH * WINDOW_HEIGHT);
//Array
int m_output[WINDOW_WIDTH][WINDOW_HEIGHT] = {0};
int m_state[WINDOW_WIDTH][WINDOW_HEIGHT] = {0};

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Color grid_background = {22, 22, 22, 255};      // Barely Black
SDL_Color grid_cursor_color = {255, 255, 255, 255}; // White

void automata(int x, int y)
{
    int nVizinhos = m_output[(x - 1)][(y - 1)] + m_output[(x - 0)][(y - 1)] + m_output[(x + 1)][(y - 1)] +
                    m_output[(x - 1)][(y - 0)] + 0 + m_output[(x + 1)][(y - 0)] +
                    m_output[(x - 1)][(y + 1)] + m_output[(x - 0)][(y + 1)] + m_output[(x + 1)][(y + 1)];

    if (m_output[x][y] == 1)
        m_state[x][y] = (nVizinhos == 2 || nVizinhos == 3) ? 1 : 0;
    else
        m_state[x][y] = (nVizinhos == 3) ? 1 : 0;
}

void barreira()
{
    {
        pthread_mutex_lock(&mut);
        counter++;
        pthread_mutex_unlock(&mut);
    }
    // printf("entrou na barreira\n");

    if (counter == THREADSTOTAL)
    {
        counter = 0;
        drawRoutine();
        pthread_cond_broadcast(&cond);
        // printf("unlock\n");
    }
    else
    {
        pthread_cond_wait(&cond, &bar);
        pthread_mutex_unlock(&bar);
    }

    // printf("Saiu da barreira\n");
}

//problema
void *exc(void *arg)
{
    t_Args *args = (t_Args *)arg;
    while (!quit)
    {
        pthread_mutex_lock(&cnt);
        itCounter++;
        // printf("%d\n",itCounter);
        pthread_mutex_unlock(&cnt);

        for (int i = (args->start); i < (args->end); i++)
        {
            for (int j = 1; j < WINDOW_HEIGHT; j++)
            {
                automata(i, j);
            }
        }

        barreira();
    }

    // printf("\n\n\nsaida\n\n\n");
    pthread_exit(NULL);
    return 0;
}

//problema2
void *exc2(void *arg)
{
    free(arg);
    while (itCounter < ITER)
    {

        pthread_mutex_lock(&mut);
        if (x == WINDOW_WIDTH - 1)
        {
            y++;
            x = 0;
        }
        if (y == WINDOW_HEIGHT)
        {
            pthread_mutex_lock(&cnt);
            itCounter++;
            pthread_mutex_unlock(&cnt);
            // printf("WH: %d,%d\n", x, y);
            x = 1;
            y = 1;
            drawRoutine();
        }
        int x1 = x++;
        int y1 = y;
        pthread_mutex_unlock(&mut);

        automata(x1, y1);
    }

    pthread_exit(NULL);
    return 0;
    
}

void drawRoutine()
{
    // printf("Desenho\n");
    for (int i = 0; i < WINDOW_WIDTH; i++)
    {
        for (int j = 0; j < WINDOW_HEIGHT; j++)
        {
            m_output[i][j] = m_state[i][j];
        }
    }
    // Desenha o fundo
    SDL_SetRenderDrawColor(renderer, grid_background.r, grid_background.g,
                           grid_background.b, grid_background.a);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, grid_cursor_color.r,
                           grid_cursor_color.g, grid_cursor_color.b,
                           grid_cursor_color.a);
    //Desenha os quadrados
    for (int i = 1; i < WINDOW_WIDTH; i++)
    {
        for (int j = 1; j < WINDOW_HEIGHT; j++)
        {

            if (m_state[i][j] == 1)
            {
                SDL_Rect gd;
                gd.x = i * CELL_SIZE + 1;
                gd.y = j * CELL_SIZE + 1;
                gd.w = CELL_SIZE;
                gd.h = CELL_SIZE;
                SDL_RenderFillRect(renderer, &gd);
            }
        }
    }

    SDL_RenderPresent(renderer);
    std::this_thread::sleep_for(std::chrono::milliseconds(DELAY));
}

void initVideo()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Initialize SDL: %s",
                     SDL_GetError());
    }
}

void createRenderer(int window_width, int window_height)
{
    if (SDL_CreateWindowAndRenderer(window_width, window_height, 0, &window,
                                    &renderer) < 0)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "Create window and renderer: %s", SDL_GetError());
    }

    SDL_SetWindowTitle(window, "Game of Life");
}

int main()
{
    pthread_t threads[THREADSTOTAL];

    pthread_mutex_init(&bar, NULL);
    pthread_mutex_init(&mut, NULL);
    pthread_mutex_init(&cnt, NULL);
    pthread_cond_init(&cond, NULL);
    int i;
    t_Args *arg;

    int window_width = (WINDOW_WIDTH * CELL_SIZE);
    int window_height = (WINDOW_HEIGHT * CELL_SIZE);

    initVideo();
    createRenderer(window_width, window_height);

    auto set = [&](int x, int y, std::wstring s) {
        int p = 0;
        for (auto c : s)
        {
            m_state[x + p][y] = c == L'#' ? 1 : 0;
            p++;
        }
    };

    // for(int i = 0; i<WINDOW_WIDTH; i++)
    // {
    //     for(int j = 0; j<WINDOW_HEIGHT;j++)
    //     {
    //         m_state[i][j] =((rand()%2 == 0) ? 1 : 0);
    //     }
    // }


    set(290,200, L"..##.");
    set(290,201, L".##..");
    set(290,202, L"..#..");

    set(200,240, L"..##.");
    set(200,241, L".##..");
    set(200,242, L"..#..");

    set(310,220, L".......#");
    set(310,221, L".##");
    set(310,222, L"..#...###");

    // set(70,60, L"..#");
    // set(70,61, L"....#");
    // set(70,62, L".##..###");

    // set(190,60, L"#####");
    // set(190,61, L"#....#.......##");
    // set(190,62, L"#...........##.###");
    // set(190,63, L".#.........##.####");
    // set(190,64, L"...##...##.##..##");
    // set(190,65, L".....#....#..#");
    // set(190,66, L"......#.#.#.#");
    // set(190,67, L".......#");
    // set(190,68, L".......#");
    // set(190,69, L"......#.#.#.#");
    // set(190,70, L".....#....#..#");
    // set(190,71, L"...##...##.##..##");
    // set(190,72, L".#.........##.####");
    // set(190,73, L"#...........##.###");
    // set(190,74, L"#....#.......##");
    // set(190,75, L"#####");

    set(60, 45, L"........................#............");
    set(60, 46, L"......................#.#............");
    set(60, 47, L"............##......##............##.");
    set(60, 48, L"...........#...#....##............##.");
    set(60, 49, L"##........#.....#...##...............");
    set(60, 50, L"##........#...#.##....#.#............");
    set(60, 51, L"..........#.....#.......#............");
    set(60, 52, L"...........#...#.....................");
    set(60, 53, L"............##.......................");

    set(260, 75, L"............#........................");
    set(260, 76, L"............#.#......................");
    set(260, 77, L".##............##......##............");
    set(260, 78, L".##............##....#...#...........");
    set(260, 79, L"...............##...#.....#........##");
    set(260, 80, L"............#.#....##.#...#........##");
    set(260, 81, L"............#.......#.....#..........");
    set(260, 82, L".....................#...#...........");
    set(260, 83, L".......................##............");

    drawRoutine();
    // printf("threads: %d\n",std::thread::hardware_concurrency());
    printf("threads: %d\n",THREADSTOTAL);

    int step = WINDOW_WIDTH / THREADSTOTAL;

    for (int i = 0; i < THREADSTOTAL; i++)
    {
        arg = (t_Args *)malloc(sizeof(t_Args));
        if (arg == NULL)
        {
            printf("--ERRO: malloc()\n");
            return -1;
        }

        arg->start = i * step;

        if(arg-> start == 0)
            arg->start = 1;
        arg->end = (i + 1) * step;

        if (arg == NULL)
        {
            printf("--ERRO: malloc()\n");
            return -1;
        }

        if (pthread_create(&threads[i], NULL, exc, (void *)arg))
        {
            printf("--ERRO: pthread_create()\n");
            return -1;
        }
    }

    while (!quit)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit = SDL_TRUE;
                break;
            }
        }
    }
    
    // getchar();

    for (auto &t : threads)
    {
        pthread_join(t, NULL);
    }
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return EXIT_SUCCESS;
}
